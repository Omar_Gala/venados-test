# VenadosTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Descarga e inicialización del proyecto (Modo DEV)

El primer paso, después de clonar el proyecto en su equipo, estando dentro de la carpeta del mismo, ejecutamos el comando `npm install`.

El siguiente paso, ejecutamos el comando `npm start`

Al realizar este último paso, obtenemos la url `http://localhost:4200/` para acceder al aplicativo.

El proyecto para modo DEV tiene configurado un proxy para consumir la api para el test (Ver archivo en raiz del proyecto: proxy.config.json). Este se ejecuta desde el momento que arrancamos el aplicativo.

Finamlemente podemos navegar en el aplicativo.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



 