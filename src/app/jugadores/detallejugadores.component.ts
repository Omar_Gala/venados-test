import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Jugador } from '../models/jugadores';

@Component({
    selector: 'detalle-jugador',
    templateUrl: '../views/detalle.html',
    styleUrls: ['./detallejugadores.component.scss']
})

export class DetallejugadoresComponent implements OnInit{
    public titulo: string;
    public jugador: any;
    public response: {};
    @Input() informacionjugador: Jugador;    
    
    constructor(){
        this.jugador = this.informacionjugador;
    }
    ngOnInit(): void {               
        console.log("jugadores",this.jugador);
    }
}