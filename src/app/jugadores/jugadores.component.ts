import {Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Jugador } from '../models/jugadores';
import { JugadoresService } from '../servicios/jugadores.service';
import { DetallejugadoresComponent } from "./detallejugadores.component";
@Component({
    selector: 'jugadores',
    templateUrl: '../views/jugadores.html',
    styleUrls: ['./jugadores.component.scss'],
    providers: [JugadoresService]
})

export class JugadoresComponent implements OnInit{
    public titulo: string;
    public jugadores: Jugador[];
    public allJugadores: Jugador[];
    public response: {};
    public dataJugador: Jugador; 
    @ViewChild(DetallejugadoresComponent) infoJugador: DetallejugadoresComponent;
    
    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private jugadoresService: JugadoresService
    ){
        this.titulo = 'Jugadores';
        console.log(this.titulo);
        this.dataJugador = {
            name: "",
            first_surname: "",
            second_surname: "",
            birthday: "",
            birth_place: "",
            weight: 0,
            height: 0,
            position: "",
            number: 0,
            position_short: "",
            last_team: "",
            image: ""
        };
    }
    ngOnInit(): void {

        this.jugadoresService.getJugadores()
        .subscribe(
            result => {
                this.response = result;
                
                if(this.response['success'])
                {  
                    this.jugadores = this.response['data']['team'];
            
                    var forwards = this.jugadores['forwards'];
                    var centers = this.jugadores['centers'];
                    var defenses = this.jugadores['defenses'];
                    var goalkeepers = this.jugadores['goalkeepers'];
                    var coaches = this.jugadores['coaches'];            

                    this.allJugadores =  forwards.concat(centers);
                    this.allJugadores = this.allJugadores .concat(defenses);
                    this.allJugadores = this.allJugadores .concat(goalkeepers);
                    this.allJugadores = this.allJugadores .concat(coaches);
                    console.log(this.allJugadores);
                }
                else
                {
                    console.log(this.response);
                }  
            },
            error => {
                console.log(<any>error);
            }
        );
       /*  this.response = this.jugadoresService.getJugadores();
        if(this.response['success'])
        {            
            this.jugadores = this.response['data']['team'];
            
            var forwards = this.jugadores['forwards'];
            var centers = this.jugadores['centers'];
            var defenses = this.jugadores['defenses'];
            var goalkeepers = this.jugadores['goalkeepers'];
            var coaches = this.jugadores['coaches'];            

            this.allJugadores =  forwards.concat(centers);
            this.allJugadores = this.allJugadores .concat(defenses);
            this.allJugadores = this.allJugadores .concat(goalkeepers);
            this.allJugadores = this.allJugadores .concat(coaches);

        }
        else
        {
            console.log(this.response);
        } */
    }
    detalleJugador(data): void{
        this.dataJugador = data;
        /* console.log(this.data); */
    }

}