export class Estadistica{
    constructor(
        public image:string,
        public team: string,
        public games: number,
        public win: number,
        public loss: number,
        public tie: number,
        public f_goals: number,
        public a_goals: number,
        public score_diff: number,
        public points: number,
        public efec: string
    ){}
}