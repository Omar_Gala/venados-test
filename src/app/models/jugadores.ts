export class Jugador{
    constructor(
        public name:string,
        public first_surname: string,
        public second_surname: string,
        public birthday: string,
        public birth_place: string,
        public weight: number,
        public height: number,
        public position: string,
        public number: number,
        public position_short: string,
        public last_team: string,
        public image: string
    ){}
}
