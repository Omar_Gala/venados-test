import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes 
import { HomeComponent } from './home/home.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { ErrorComponent } from './componentes/error.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'home', component: HomeComponent},
    {path:'estadisticas', component: EstadisticasComponent},
    {path:'jugadores', component: JugadoresComponent},
    {path: '**', component: ErrorComponent}    
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);