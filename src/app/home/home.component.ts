import {Component} from '@angular/core';
import { CopaComponent } from '../copa/copa.component'; 
import { AscensoComponent } from '../ascenso/ascenso.component';
@Component({
    selector: 'home',
    templateUrl: '../views/home.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent{
    public titulo: string;
    public showCopa: boolean;
    public showAscenso: boolean;
    
    constructor(){
        this.showAscenso = false;
        this.showCopa = true;
        this.titulo = 'Home';
        console.log(this.showCopa);
    }

    ngOninit(): void{
        console.log(this.titulo);
    }

    selectTab(tab): void{
        
        switch (tab) {
            case 'copamx':
                console.log(tab);
                // muestra copamx
                this.showCopa = true;
                this.showAscenso = false;
                
                var element = document.getElementById("ascensomx");
                element.classList.remove("active");
                var element2 = document.getElementById("copamx");
                element2.classList.add("active");
                break;
            case 'ascensomx':
                console.log(tab);
                // muestra ascenso
                this.showCopa = false;
                this.showAscenso = true;
                var element = document.getElementById("copamx");
                element.classList.remove("active");
                var element2 = document.getElementById("ascensomx");
                element2.classList.add("active");
                break;
            default:
                break;
        }
    }
}