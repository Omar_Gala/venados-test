import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; // Agregar esto siempre o da problemas 
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './componentes/sidebar.component';
import { routing, appRoutingProviders } from './app.routing';
import { ErrorComponent } from './componentes/error.component';
import { CopaComponent } from './copa/copa.component';
import { AscensoComponent} from './ascenso/ascenso.component';
import { DetallejugadoresComponent } from './jugadores/detallejugadores.component';
import { HighlightDirective } from './directivas/highlight.directive';

@NgModule({
  declarations: [
    AppComponent,
    EstadisticasComponent,
    JugadoresComponent,
    HomeComponent,
    SidebarComponent,
    ErrorComponent,
    CopaComponent,
    AscensoComponent,
    HighlightDirective,
    DetallejugadoresComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    routing
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
