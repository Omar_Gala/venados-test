import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class JuegosService{
        
    constructor(        
        private _httpClient: HttpClient    
    ){
       
    } 

    getJuegos(){
        let headers_ = new HttpHeaders({
            'Accept': 'application/json'            
        });

        return this._httpClient.get<any>('/api/games',{headers:headers_});     
    }
 
}
