import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class EstadisticasService{

    constructor(
        private _httpClient: HttpClient  
    ){
      
    } 

    getEstadisticas(){
        let headers_ = new HttpHeaders({
            'Accept': 'application/json'            
        });

        return this._httpClient.get<any>('/api/statistics',{headers:headers_});     
    }
 
}
