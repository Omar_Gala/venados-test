import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class JugadoresService{
    
    constructor(
        private _httpClient: HttpClient    
    ){
       
    } 

    getJugadores(){
        let headers_ = new HttpHeaders({
            'Accept': 'application/json'            
        });

        return this._httpClient.get<any>('/api/players',{headers:headers_});     
    }
 
}
