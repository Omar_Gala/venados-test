import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { EstadisticasService } from '../servicios/estadisticas.service';
import { Estadistica } from '../models/estadisticas';

@Component({
    selector: 'estadisticas',
    templateUrl: '../views/estadisticas.html',
    styleUrls: ['./estadisticas.component.scss'],
    providers: [EstadisticasService]
})

export class EstadisticasComponent implements OnInit{
    public titulo: string;    
    public estadisticas: Estadistica[];
    public response: {};

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private estadisticasService: EstadisticasService
    ){
        this.titulo = 'Estadísticas';
        console.log(this.titulo);        
    }
    ngOnInit(): void {
        console.log(this.titulo);

        this.estadisticasService.getEstadisticas()
            .subscribe(
                result => {       
                    
                    this.response = result;

                    if(this.response['success'])
                    {
                        this.estadisticas = this.response['data']['statistics'];                                                
                    }
                    else
                    {
                        console.log(this.response);
                    }                                
                },
                error => {
                    console.log(<any>error);
                }
            );
    } 
}