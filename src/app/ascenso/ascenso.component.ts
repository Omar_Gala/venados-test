import { Component, OnInit } from '@angular/core';
import { JuegosService } from '../servicios/juegos.service';
import { Juego } from '../models/juegos';

@Component({
    selector: 'ascenso',
    templateUrl: '../views/ascenso.html',
    styleUrls: ['./ascenso.component.scss'],
    providers: [JuegosService]
})

export class AscensoComponent implements OnInit{
    public titulo: string;     
    public juegos: Juego[];  
    public filteredJuegos: Juego[];  
    public _filteredJuegos: Juego[];  
    public arrayJuegos: any;
    public juegos_mes: Juego;
    public meses: any; 
    public response: {};
    public banderaMes: boolean;
    
    constructor(
        private juegosService: JuegosService,
    ){        
        this.titulo = 'Ascenso MX';    
        this.meses = [
            {
                'id' : 1,
                'mes' : 'ENERO'
            },
            {
                'id' : 2,
                'mes' : 'FEBRERO'
            },
            {
                'id' : 3,
                'mes' : 'MARZO'
            },
            {
                'id' : 4,
                'mes' : 'ABRIL'
            },
            {
                'id' : 5,
                'mes' : 'MAYO'
            },
            {
                'id' : 6,
                'mes' : 'JUNIO'
            },
            {
                'id' : 7,
                'mes' : 'JULIO'
            },
            {
                'id' : 8,
                'mes' : 'AGOSTO'
            },
            {
                'id' : 9,
                'mes' : 'SEPTIEMBRE'
            },
            {
                'id' : 10,
                'mes' : 'OCTUBRE'
            },
            {
                'id' : 11,
                'mes' : 'NOVIEMBRE'
            },
            {
                'id' : 12,
                'mes' : 'DICIEMBRE'
            }
        ];    
    }
    ngOnInit(): void {        
        console.log(this.titulo);

        this.juegosService.getJuegos()
            .subscribe(
                result => {       
                    
                    this.response = result;

                    if(this.response['success'])
                    {
                        this.juegos = this.response['data']['games'];
                        var league = this.titulo;
                        this.filteredJuegos = this.juegos.filter(option => option.league.indexOf(league)===0);

                        // Después de filtrar agrupamos
                        // this.juegos_mes
                        var juegos_key_mes = [];
                        
                        var last_month = 0;
                        this.filteredJuegos.forEach((item, index) => {  
                            console.log('index', index);
                            var dateJuego = new Date(item.datetime); 
                            var month = dateJuego.getMonth();                            
                            var nameMonth = "";
                            month = month+1;
                            if(index == 0){
                                nameMonth = this.getMonth(month);  
                                var obj = {
                                    'id'  : month,
                                    'mes' : nameMonth,
                                    'elements' : []
                                }
                                juegos_key_mes.push(obj);
                                last_month = month;
                                console.log(nameMonth);                              
                            }
                            else
                            {
                                if(last_month < month)
                                {
                                    nameMonth = this.getMonth(month); 
                                    var obj = {
                                        'id'  : month,
                                        'mes' : nameMonth,
                                        'elements' : []
                                    }
                                     
                                    juegos_key_mes.push(obj);
                                    last_month = month;
                                    console.log(nameMonth);
                                }
                                
                            }                            
                            
                        })   
                        this.filteredJuegos.forEach((item, index) => {
                            var dateJuego = new Date(item.datetime); 
                            var month = dateJuego.getMonth();         
                            juegos_key_mes.forEach(key_mes=>{
                                if(key_mes.id == month+1)
                                {
                                    key_mes.elements.push(item);
                                }
                            })
                        });
                        console.log(juegos_key_mes);
                        this.arrayJuegos = juegos_key_mes;                                      
                    }
                    else
                    {
                        console.log(this.response);
                    }                                
                },
                error => {
                    console.log(<any>error);
                }
            );

    }

    getMonth(month): string {
        var name_month = "";
        this.meses.forEach(element => {
            if(month == element.id)
            {
                name_month = element.mes;
            }
        });
        return name_month;

    }
    validaJuegosMes(fecha): string {
       
       return "mes";
    }
    getNumberDate(date): number{
        console.log(date);


        var date_ = new Date(date); 
        var numberDate = date_.getDate();         
        return numberDate;
    }
    getDayDate(date): string{
        console.log(date);
        // 0 es lunes y 6 es domingo
        var date_ = new Date(date); 
        var dayDate  = date_.getDay();   
        var nameDate = "";
        
        switch (dayDate) {
            case 0:
                nameDate = "LUN";
                break;
            case 1:
                nameDate = "MAR";
                break;
            case 2:
                nameDate = "MIE";
                break;
            case 3:
                nameDate = "JUE";
                break;
            case 4:
                nameDate = "VIE";
                break;
            case 5:
                nameDate = "SAB";
                break;
            case 6:
                nameDate = "DOM";
                break;
        
            default:
                break;
        }

        return nameDate;
    }
}