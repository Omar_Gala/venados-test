import {Component} from '@angular/core';

@Component({
    selector: 'sidebar',
    templateUrl: '../views/sidebar.html',
    styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent{
    public titulo: string;
    
    constructor(){
        this.titulo = 'Sidebar';
        console.log(this.titulo);
    }

    ngOninit(): void{
        console.log(this.titulo);
    }
    openSideBar(): void 
    {   
        document.getElementById("sidebarnav").style.width = "400px";
        document.getElementById("main").style.marginLeft = "0px";
    }
    closeSideBar(): void 
    {   
        document.getElementById("sidebarnav").style.width = "0px";
        document.getElementById("main").style.marginLeft = "0px";
    }
}